const express = require("express");
const http = require("http");
const socketIO = require("socket.io")

const serverPath = "F:\\MinecraftServer"
const startServerCommand = ["java", ["-Xmx1024M", "-Xms1024M", "-jar", `${serverPath}\\server.jar`, "nogui"]];
const port = 3000;

const app = express();
const server = http.createServer(app);
const socketServer = socketIO(server);

const logRegex = {
    port: /^\[.+\] \[.+\]: Starting Minecraft server on \*:(\d+)$/,
    spawnAreaProgress: /^\[.+\] \[.+\]: Preparing spawn area: (\d+)%$/,
    started: /^\[.+\] \[.+\]: Done \((\d+\.\d+)s\)! For help, type "help"$/
}

function extractPort(log) {
    let regexResult = logRegex.port.exec(log);
    return regexResult ? regexResult[1] : null;
}

function extractSpawnAreaProgress(log) {
    let regexResult = logRegex.spawnAreaProgress.exec(log);
    return regexResult ? regexResult[1] : null;
}

function extractSuccessMessage(log) {
    let regexResult = logRegex.started.exec(log);
    return regexResult ? regexResult[1] : null;
}

socketServer.origins('*:*')

app.get("/startServer", (req, res) => {
    startMinecraftServer();
    res.send("Staring now :)")
});

socketServer.on("connection", socket => {
    console.log("a client connected")

    if (minecraft) {
        socketServer.emit("serverStarted");
    } else {
        socketServer.emit("serverStopped");
    }

    socket.on("disconnect", socket => {
        console.log("a client disconnected")
    })

    socket.on("startServer", () => {
        startMinecraftServer();
    })

    socket.on("stopServer", () => {
        stopMinecraftServer();
    })

    socket.on("getPort", socket => {
        console.log("getport")
        socketServer.emit("port", mcPort);
    })
})

server.listen(port, () => console.log(`Now listening on port ${port}`));

var minecraft;

var mcPort = undefined;

function startMinecraftServer() {
    if (!minecraft) {

        minecraft = require('child_process').spawn(...startServerCommand, {
            cwd: serverPath
        });

        minecraft.on('exit', function (code, signal) {
            console.log(`child process exited with code ${code} and signal ${signal}`);
            socketServer.emit("message", "SERVER STOPPED SUCCESSFULLY")
            socketServer.emit("serverStopped")
            minecraft = undefined;
            mcPort = undefined;
        });

        minecraft.stdout.on("data", data => {
            let log = data.toString().trim();
            socketServer.emit("message", log);

            // serverProgress.port = extractPort(log) || serverProgress.port;
            // serverProgress.spawnAreaProgress = extractSpawnAreaProgress(log) || serverProgress.port;
            // if (extractSpawnAreaProgress(log)) {
            //     serverProgress.starting = false;
            //     serverProgress.running = true;
            //     serverProgress.bootTime = +extractSpawnAreaProgress(log);
            // }

            if (extractSuccessMessage(log)) {
                socketServer.emit("serverStarted")
            }

            let porty = extractPort(log)
            if (porty) {
                mcPort = porty;
                socketServer.emit("port", mcPort);
            }
        })

        process.stdin.pipe(minecraft.stdin);
    }
}

function stopMinecraftServer() {
    if (minecraft) {
        minecraft.stdin.write("stop");
        minecraft.stdin.end();
    }
}