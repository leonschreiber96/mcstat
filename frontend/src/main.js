import Vue from 'vue'
import App from './App.vue'
import VueSocketIO from 'vue-socket.io'
import SocketIO from 'socket.io-client'

export const SocketInstance = SocketIO.connect('http://localhost:3000')

Vue.use(VueSocketIO, SocketInstance)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
